#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/chkconfig/Sanity/backport-family-option-from-upstream
#   Description: Test for BZ#1291340 (Backport --family option from upstream)
#   Author: Jan Scotka <jscotka@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="chkconfig"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "touch $TmpDir/a"
        rlRun "touch $TmpDir/b"
        rlRun "ln -s $TmpDir/a $TmpDir/link"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "alternatives --install $TmpDir/link testname $TmpDir/a 1 --family testfamily"
        rlRun "alternatives --install $TmpDir/link testname $TmpDir/b 2 --family testfamily"
        rlRun "alternatives --display testname |grep 'link.*$TmpDir/b'" 
        rlRun "alternatives --display testname |grep 'link.*$TmpDir/a'" 1
        rlRun "alternatives --display testname |grep '$TmpDir/a.*testfamily priority 1'"
        rlRun "alternatives --list | grep 'testname.*$TmpDir/b'"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun " alternatives --remove testname $TmpDir/a"
        rlRun " alternatives --remove testname $TmpDir/b"
        rlRun "rm -rf $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
