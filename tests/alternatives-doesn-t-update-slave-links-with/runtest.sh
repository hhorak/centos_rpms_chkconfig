#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/chkconfig/Regression/alternatives-doesn-t-update-slave-links-with
#   Description: Test for BZ#1347541 (alternatives doesn't update slave links with)
#   Author: Jan Scotka <jscotka@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="chkconfig"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
    rlPhaseEnd
    rlPhaseStartTest

        rlRun "alternatives --install /usr/bin/xxx xxx /usr/bin/true 1 --slave /usr/bin/yyy yyy /usr/bin/true"
        rlRun "alternatives --set xxx /usr/bin/true"
        rlRun "alternatives --install /usr/bin/xxx xxx /usr/bin/true 1 --slave /usr/bin/yyy yyy /usr/bin/false"
        rlRun "readlink /etc/alternatives/yyy | grep /usr/bin/false"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "update-alternatives --remove xxx /usr/bin/true"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
